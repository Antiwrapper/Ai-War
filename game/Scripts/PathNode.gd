extends StaticBody2D

var ConnectedNodes = [];
var Nodes = [];

func _ready():
	add_to_group("PathNodes");
	set_fixed_process(false);

func _update_connections():
	ConnectedNodes = [];
	Nodes = get_tree().get_nodes_in_group("PathNodes");
	set_fixed_process(true);

func _fixed_process(delta):
	var space = get_world_2d().get_direct_space_state();

	for n in Nodes:
		if (n != self):
			var result = space.intersect_ray(get_pos(), n.get_pos());
			if (result.empty() == true):
				ConnectedNodes.push_back(n);
	update();
	set_fixed_process(false);

func search(node, parent):
	if (node == self):
		return [self];
	
	for n in ConnectedNodes:
		if (n == node):
			return [self, node];
	
	#for n in ConnectedNodes:
	#	if (n != parent):
	#		var result = n.search(node, self);
	#		if (result != []):
	#			result.push_front(self);
	#			return result;
	return [];

func _draw():
	for n in ConnectedNodes:
		draw_line(Vector2(0, 0), n.get_pos() - get_pos(), Color(0.9, 0.9, 1.0, 1.0), 2);