extends KinematicBody2D

var Dir = Vector2();
var Speed = 400;

func _ready():
	set_fixed_process(true);

func _fixed_process(delta):
	move(Speed * Dir * delta);
	if (is_colliding() == true):
		if (get_collider().has_method("_shoot")):
			get_collider().Health -= 1;
		queue_free();