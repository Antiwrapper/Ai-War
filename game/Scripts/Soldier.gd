extends KinematicBody2D

const THRESHOLD = 2;
const COOLDOWN = 0.5;

var Begin = Vector2();
var End = Vector2();
var Enemy = Vector2();
var Speed = 200;
var PathToEnd = [];
var Red = false;
var Health = 1;
var Dir = Vector2();
var Velocity = Vector2();
var Cooldown = COOLDOWN;
var Attack = false;

func _ready():
	if (get_pos().x < OS.get_window_size().width / 2):
		get_node("Sprite").set_modulate(Color(1, 0, 0, 1));
		add_to_group("Red");
		Red = true;
	else:
		get_node("Sprite").set_modulate(Color(0, 0, 1, 1));
		add_to_group("Blue");
	set_process(false);
	set_fixed_process(true);
	_update_path(Vector2(rand_range(64, 940), rand_range(64, 700)));

func _process(delta):
	if PathToEnd.size() > 0:
		Dir = (PathToEnd[0] - get_pos()).normalized();
		Velocity = Dir * Speed;
		if ((PathToEnd[0] - get_pos()).length() < THRESHOLD):
			PathToEnd.remove(0);
	else:
		Velocity = Vector2(0, 0);
		set_process(false);

func _fixed_process(delta):
	var space = get_world_2d().get_direct_space_state();
	var Enemies = [];
	var Friends = [];
	if (Cooldown >= 0):
		Cooldown -= delta;
	
	if (Health < 1):
		queue_free();
	
	var motion = move(Velocity * delta);
	if (is_colliding() == true):
		var n = get_collision_normal();
		motion = n.slide(motion);
		move(motion);
	
	if (Red == false):
		Enemies = get_tree().get_nodes_in_group("Red");
		Friends = get_tree().get_nodes_in_group("Blue");
	else:
		Enemies = get_tree().get_nodes_in_group("Blue");
		Friends = get_tree().get_nodes_in_group("Red");
	if (Enemies.size() - Friends.size() < -4 && Attack == false):
		Attack = true;
		if (Red == false):
			_update_path(get_parent().get_node("Base").get_pos());
		else:
			_update_path(get_parent().get_node("Base1").get_pos());
		
	for e in Enemies:
		Enemy = e.get_pos();
		var result = space.intersect_ray(get_pos() + Vector2(0, -16), e.get_pos(), [self, e]);
		if (result.empty() == true):
			if (Cooldown < 0):
				#_shoot((e.get_pos() - get_pos()).normalized());
				Cooldown = COOLDOWN;

func _find_node(pos):
	var nodes = get_tree().get_nodes_in_group("PathNodes");
	var closest = null;
	var dist = 999999.9;
	
	for n in nodes:
		if ((n.get_pos() - pos).length() < dist):
			closest = n;
			dist = (n.get_pos() - pos).length();
	return closest;

func _update_path(end):
	PathToEnd = [];
	Begin = get_pos();
	End = end;
	
	var node_start = _find_node(Begin);
	var node_end = _find_node(End);
	
	if (node_start != null && node_end != null):
		var temp = node_start.search(node_end, null);
		if (temp != []):
			for n in temp:
				PathToEnd.push_back(n.get_pos());
	
	set_process(true);

func _shoot(dir):
	var b = preload("res://Scenes/Bullet.tscn").instance();
	b.set_pos(get_pos() + Vector2(0, -16));
	add_collision_exception_with(b);
	b.add_collision_exception_with(self);
	b.Dir = dir;
	get_parent().add_child(b);