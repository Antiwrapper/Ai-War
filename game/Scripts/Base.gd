extends Sprite

const TIMER = 2;

var Time = TIMER;

func _ready():
	randomize();
	var s = preload("res://Scenes/Soldier.tscn").instance();
	s.set_pos(get_pos());
	get_parent().call_deferred("add_child", s);
	
	if (get_pos().x < OS.get_window_size().width / 2):
		set_modulate(Color(1, 0, 0, 1));
	else:
		set_modulate(Color(0, 0, 1, 1));
	
	set_process(true);

func _process(delta):
	Time -= delta;
	if (Time < 0):
		Time = TIMER;
		var s = preload("res://Scenes/Soldier.tscn").instance();
		s.set_pos(get_pos());
		get_parent().add_child(s);
