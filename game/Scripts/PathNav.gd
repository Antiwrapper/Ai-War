extends Node2D

const GAP_WIDTH = 128;
const GAP_HEIGHT = 128;

var NumNodes = 0;

func _ready():
	set_fixed_process(true);

func _fixed_process(delta):
	var x = 0;
	var y = 0;
	var space = get_world_2d().get_direct_space_state();
	
	while (x < OS.get_window_size().width):
		while (y < OS.get_window_size().height):
			var result = space.intersect_point(Vector2(x, y), 1);
			if (result.size() == 0):
				var p = preload("res://Scenes/PathNode.tscn").instance();
				p.set_pos(Vector2(x, y));
				get_parent().add_child(p);
				NumNodes += 1;
			y += GAP_HEIGHT;
		x += GAP_WIDTH;
		y = 0;
	
	get_tree().call_group(0, "PathNodes", "_update_connections");
	print(NumNodes);
	
	set_fixed_process(false);